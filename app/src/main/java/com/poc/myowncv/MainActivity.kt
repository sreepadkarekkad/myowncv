package com.poc.myowncv

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.SeekBar
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONArray
import java.io.IOException
import java.io.InputStream

class MainActivity : AppCompatActivity() {

    var arr = arrayListOf<String>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        readJson()
    }
    fun readJson() {
        var json:  String? = null
        try {
            val inputStream: InputStream = assets.open("resume.json")
            json = inputStream.bufferedReader().use { it.readText() }
            var jsonArray = JSONArray(json)
            for(i in 0..jsonArray.length()-1) {
                var jsonObj = jsonArray.getJSONObject(i)
                arr.add(jsonObj.getString("name"))
            }
            var adpt = ArrayAdapter(this,android.R.layout.simple_list_item_1,arr)
            json_list.adapter = adpt
        }catch(e:IOException) {}
    }
}
